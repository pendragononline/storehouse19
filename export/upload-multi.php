<?php 
//Set system timezone
date_default_timezone_set("Europe/London");
//General variables
$studentid = $_POST['idfield'];
$name = $_POST['namefield'];
$year = $_POST['yeargroup'];
$course = $_POST['course'];
$emailaddress = $_POST['emailaddress'];
$instagram = $_POST['instagramname'];
$title = $_POST['submissiontitle'];
$youtube = $_POST['youtubeurl'];
$articlecontent = $_POST['articlewords'];
$date = date("d/m/Y H:i");
$courseDirName = strtolower(preg_replace("/[^A-Za-z0-9]/", '-', $course));
$titleDirName = strtolower(preg_replace("/[^A-Za-z0-9]/", '-', $title));
$nameDirName = strtolower(preg_replace("/[^A-Za-z0-9]/", '-', $name));
$uploadDir = "submissions/sh20/" . $nameDirName . "-yr" . $year . "-" . $courseDirName . "-" . $titleDirName .  "-sh20/";

$files = array_filter($_FILES['upload']['name']); 

//Create sub-dir for each article
if (file_exists($uploadDir)) {
	header("Location: http://storehouse.online/submissions/help.html"); 
	die();
}
else {
	mkdir($uploadDir, 0777);
}

// Count # of uploaded files in array
$total = count($_FILES['upload']['name']);

// Loop through each file
for( $i=0 ; $i < $total ; $i++ ) {

	//Get the temp file path
	$tmpFilePath = $_FILES['upload']['tmp_name'][$i];

	//Make sure we have a file path
	if ($tmpFilePath != ""){
	//Setup our new file path
		$newFilePath = $uploadDir . $_FILES['upload']['name'][$i];

	//Upload the file into the temp dir
		if(move_uploaded_file($tmpFilePath, $newFilePath)) {

			//Handle other code here
		}
	}
}

$infofile = fopen($uploadDir . $nameDirName . '-info.txt', "w") or die("Error creating the information file to accompany the submission. It may be because your article title contains an invalid character (e.g. a / ).");
$text = "Submission for Issue 20 received on: " . $date . "\n\nStudent information\nName: " . $name . "\nStudent ID: " . $studentid . "\nYear: " . $year . "\nCourse: " . $course . "\nNUA Email: " . $emailaddress . "\nSubmission title: " . $title . "\nInstagram account: " . $instagram . "\nYouTube URL: " . $youtube . "\n\n";
fwrite($infofile, $text);
//Article contents (as Word doc)
$articlecontentfile = fopen($uploadDir. $title . ".doc", "w") or die("Error creating the Word document file containing the article contents.");
$articletext = $articlecontent;
fwrite($articlecontentfile, $articletext);
//Email Storehouse
mail('su.storehouse@nua.ac.uk', 'New submission for Storehouse Issue 20 from '. $name, $text , "From: Storehouse Issue 20 Submissions");
//Email uploader
mail($emailaddress, "Thank you for submitting work to Storehouse, " . $name . "!", "Hi, " . $name . ", thanks for submitting your work to Storehouse! Storehouse is made possible because people like you send us amazing work to publish in it! Please keep this email as receipt of submitting work. PLEASE DO NOT RESPOND TO THIS EMAIL. If you need to contact us, email su.storehouse@nua.ac.uk instead!\n\n" . $text , "From: Storehouse Issue 20 Submissions");
success();

function success() {
	header("Location: http://storehouse.online/submissions/success.html"); 
	die();
}

?>


