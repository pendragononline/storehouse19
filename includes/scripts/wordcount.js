$(document).ready(function () {
    $("#articlewords").on('keyup', function () {
        var words = this.value.match(/\S+/g).length;

        if (words > 500) {
            var trimmed = $(this).val().split(/\s+/, 500).join(" ");
            $(this).val(trimmed + " ");
            $('#articlewords').addClass("invalid-field");
            $('#wordcount-warning').removeClass("hidden");
        }
        else {
            $('#display_count').text(words);
            $('#word_left').text(500 - words);
        }
    });
}); 