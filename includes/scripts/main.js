$(document).ready(function () {
    let menu = document.querySelector('.nav .hidden-nav');
    let navo = document.querySelector('.nav-sub');
    let closenav = document.querySelector('.close-nav .con');
    let hheader = $('#header');
    let $window = $(window);
    let isTweening = false;
    let who = $('#who');
    let arrowdown = $('.arrow');
    let headerScroll = new TimelineMax({ paused: true, reversed: true });
    let cardanim = new TimelineMax({ paused: true, reversed: true });
    let cardc = $('.card-info');
    let body = document.querySelector('body');
    let links = document.querySelector('.nav-sub .con-wrap .col');
    let toggleMenu = new TimelineMax({ paused: true, reversed: true });
    let arrowtoggle = new TimelineMax;

    $('#issue16').on('mouseover', function () {
        $('.nav-sub .bg').css("background-image", "url(http://storehouse.online/includes/images/covers/issue16.jpg)");
        console.log('issue 16');
    });
    $('#issue17').on('mouseover', function () {
        $('.nav-sub .bg').css("background-image", "url(http://storehouse.online/includes/images/covers/issue17.jpg)");
        console.log('issue 17');
    });
    $('#issue18').on('mouseover', function () {
        $('.nav-sub .bg').css("background-image", "url(http://storehouse.online/includes/images/covers/issue18.jpg)");
    });
    $('#issue19').on('mouseover', function () {
        $('.nav-sub .bg').css("background-image", "url(http://storehouse.online/includes/images/covers/issue19.jpg)");
    });

    /*Issue Links*/

    $('#issue16').on('click', function () {
        location.href = 'http://storehouse.online/issue16.html';
    });
    $('#issue17').on('click', function () {
        location.href = 'http://storehouse.online/issue17.html';
    });
    $('#issue18').on('click', function () {
        location.href = 'http://storehouse.online/issue18.html';
    });
    $('#issue19').on('click', function () {
        location.href = 'http://storehouse.online/issue19.html';
    });

    // When the user scrolls down 20px from the top of the document, show the button
    window.onscroll = function () { scrollFunction() };

    function scrollFunction() {
        if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
            document.getElementById("myBtn").style.display = "block";
        } else {
            document.getElementById("myBtn").style.display = "none";
        }
    }

    // When the user clicks on the button, scroll to the top of the document
    function topFunction() {
        document.body.scrollTop = 0; // For Safari
        document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
    }

    $('#myBtn').on('click', function () {
        topFunction();
    });


    toggleMenu
        .to(navo, 1, {
            scale: 1,
            transformOrigin: "50% 0%",
            borderRadius: 0,
            ease: Power2.easeInOut
        })

        .staggerTo(".nav-sub .con-wrap .issue", 0.25, {
            scale: 1,
            opacity: 1,
            ease: Power2.easeInOut
        }, .110);

    $(menu).on('click', _ => {
        toggleMenu.reversed() ? toggleMenu.play() : toggleMenu.reverse();
    });

    $(closenav).on('click', _ => {
        toggleMenu.reversed() ? toggleMenu.play() : toggleMenu.reverse();
    });

    arrowtoggle
        .to(arrowdown, 1, {
            opacity: 1,
            ease: Power2.easeInOut
        }, 1.5);

    let scrollTop = 0;
    $(window).scroll(function () {
        scrollTop = $(window).scrollTop();
        $('.counter').html(scrollTop);

        if (scrollTop >= 100) {
            $('.header').addClass('nav-c');
            $('.logo').addClass('logo-c');
        } else if (scrollTop < 100) {
            $('.header').removeClass('nav-c');
            $('.logo').removeClass('logo-c');
        }

    });


    $('.owl-carousel').owlCarousel({
        loop: true,
        margin: 10,
        responsiveClass: true,
        autoplay: true,
        responsive: {
            0: {
                items: 1,
                nav: true
            },
            600: {
                items: 3,
                nav: false
            },
            1000: {
                items: 3,
                nav: true,
            }
        }
    });

});