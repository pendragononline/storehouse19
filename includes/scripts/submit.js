$(document).ready(function () {
    $('#uploadingMessage').hide();
    function uploadMessage() {
        verify();
    }
    var messageCounter = 0;
    var messages = [
        "Sending awesomeness!",
        "All part of the process",
        "Hang on in there!",
        "Sending, sending, 1-2-3!",
        "Creative juices flowing!",
        "On its way!",
        "Awesomeness in action!",
        "Sliding into our DMs!",
        "You've got this!"
    ];
    function changeMessage() {
        if (messageCounter > 8) {
            messageCounter = 0;
        }
        document.getElementById("message").innerHTML = messages[messageCounter];
    }
    setInterval(function () {
        messageCounter++;
        changeMessage();
    }, 7000);
    $('#submitButton').click(function () {
        uploadMessage();
    });
    function verify() {
        var student_number = document.getElementById("idfield").value.toString();
        var address = document.getElementById("emailaddress").value.toString();
        var addressdomain = "@student.nua.ac.uk";
        var staff_alumni_message = "NUA staff and alumni, please see instructions for submitting at the bottom of the submission brief.";
        var help_support = "For help and support, please see the link at the bottom of the submission brief.\n\nTip: fields with a red background contain invalid information!";
        if (document.getElementById("namefield").value === "") {
            window.alert("You must provide your name with your submission.\n\n" + help_support);
            window.reload();
        }
        if (document.getElementById("idfield").value === "" || student_number.length < 7 || student_number.length > 8) {
            window.alert("The student ID you have provided is invalid.\n\nYour student ID can be found on your NUA student card.\n\n" + help_support);
            window.reload();
        }
        if (document.getElementById("yeargroup").value === "") {
            window.alert("You must select your year group from the list.\n\n" + staff_alumni_message + "\n\n" + help_support);
            window.reload();
        }
        if (document.getElementById("course").value === "") {
            window.alert("You must select your course from the list.\n\n" + staff_alumni_message + "\n\n" + help_support);
            window.reload();
        }
        if (document.getElementById("submissiontitle").value === "") {
            window.alert("You must provide a title for your submission.\n\n" + help_support);
            window.reload();
        }
        if (document.getElementById("emailaddress").value === "" || address.includes(addressdomain) === false) {
            window.alert("You must provide your NUA student email address with your submission.\n\n" + staff_alumni_message + "\n\n" + help_support);
            window.reload();
        }
        if (document.getElementById("articlewords").value === "") {
            window.alert("You must supply a description (of up to 500 words) with your submission.\n\n" + help_support);
            window.reload();
        }
        if (document.getElementById("accept").checked === false) {
            window.alert("You must accept the Storehouse terms & conditions in order to submit to Storehouse.\n\n" + help_support);
            window.reload();
        }
        else {
            $('#submissionForm').slideUp();
            $('#uploadingMessage').slideDown();
        }
    }
});