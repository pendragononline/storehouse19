var colours = ['#030aaf', '#9c0808', '#087802', '#c43205', '#1a1a1a', '#6b0285', '#a08b00', '#008fa9', '#ff0006', '#ff9b0b', '#008fa9', '#6b0285', '#0ff23a'];
var position = ['top', 'bottom'];
var direction =['left', 'right'];

function getRandomInt(max) {
    return Math.floor(Math.random() * Math.floor(max));
}

function generateGradient() {
    let documentBG = document.querySelector("#newa");
    documentBG.style.backgroundImage = "linear-gradient(to "+position[getRandomInt(1)]+" "+direction[getRandomInt(1)]+", "+colours[getRandomInt(12)]+", "+colours[getRandomInt(12)]+")";
}

//Execute on page load
generateGradient();