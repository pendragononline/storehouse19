$(document).ready(function () {

    var animationentrances = ['bounce-in-right-animation', 'bounce-in-left-animation', 'bounce-in-up-animation', 'bounce-in-down-animation', 'lightspeed-in-animation', 'zoom-in-down-animation', 'zoom-in-animation', 'zoom-in-up-animation', 'zoom-in-left-animation', 'zoom-in-right-animation', 'rotate-in-animation'];

    function getRandomInt(max) {
        return Math.floor(Math.random() * Math.floor(max));
    }

    $('h1, h2, h3, h4, li').addClass(animationentrances[getRandomInt(10)]);
    $('.im-two').addClass(animationentrances[getRandomInt(10)]);
    $('.im-one').addClass(animationentrances[getRandomInt(10)]);
    $('.img-full').addClass(animationentrances[getRandomInt(10)]);
    $('.author').addClass(animationentrances[getRandomInt(10)]);
    $('.desc').addClass(animationentrances[getRandomInt(10)]);
    $('.video-con').addClass(animationentrances[getRandomInt(10)]);
    $('iframe').addClass(animationentrances[getRandomInt(10)]);
    $('#submitWork').addClass(animationentrances[getRandomInt(10)]);

    $('.links').addClass("animated "+animationentrances[getRandomInt(10)]);
    $('.course').addClass("animated "+animationentrances[getRandomInt(10)]);
});